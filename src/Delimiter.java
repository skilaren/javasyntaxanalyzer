class Delimiter extends Token {

    final String delimiter;

    Delimiter(String d, int line, int pos) {
        super(Tag.DELIMITER, line, pos);
        delimiter = d;
    }
}
