import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Operator extends Token {
    final String operator;
    public final static Set<String> operatorsSet = Set.of("=", ">", "<", "!", "~", "?", ":", "->", "==", ">=", "<=",
            "!=", "&&", "||", "++", "--", "+", "-", "*", "/", "&", "|", "^", "%", "<<", ">>", ">>>", "+=", "-=", "*=",
            "/=", "&=", "|=", "^=", "%=", "<<=", ">>=", ">>>=");

    Operator(String op, int line, int pos) {
        super(Tag.OPERATOR, line, pos);
        operator = op;
    }
}
