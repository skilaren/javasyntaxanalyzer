import org.w3c.dom.ls.LSOutput;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        LexicalAnalyzer lexer = new LexicalAnalyzer();
        while(true) {
            Token token = lexer.getNextToken();
            if(token.tag == Tag.EOF) {
                break;
            }
            System.out.println("| N E W      T O K E N |");
            if (token instanceof Num) {
                System.out.println("Numerical");
                System.out.println(((Num) token).value);
            }
            if (token instanceof Word) {
                if (((Word) token).type.equals("keyword"))
                    System.out.println("Keyword");
                else
                    System.out.println("Identifier");
                System.out.println(((Word) token).lexeme);
            }
            if (token instanceof Delimiter) {
                System.out.println("Delimiter");
                System.out.println(((Delimiter) token).delimiter);
            }
            if (token instanceof Operator) {
                System.out.println("Operator");
                System.out.println(((Operator) token).operator);
            }
            if (token.tag < Tag.UNRECOGNIZED) {
                System.out.println("Some token was not recognized, take a look at a source code");
            }

            System.out.println("Line: " + token.line);
            System.out.println("Position: " + (token.pos - 1));
        }
    }
}