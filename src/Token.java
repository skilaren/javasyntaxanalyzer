class Token {
    final int tag;
    final int line;
    final int pos;

    Token(int t, int l, int p) {
        tag = t;
        line = l;
        pos = p;
    }
}
