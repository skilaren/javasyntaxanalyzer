import java.io.*;
import java.util.*;

class LexicalAnalyzer {
    private int line = 1;
    private int pos = 1;
    private char next = ' ';
    private Set<String> keywords;

    LexicalAnalyzer() {
        String[] keywordsList = {"abstract", "continue", "for", "new", "switch",
        "assert", "default", "if", "package", "synchronized",
        "boolean", "do", "goto", "private", "this",
        "break", "double", "implements", "protected", "throw",
        "byte", "else", "import", "public", "throws",
        "case", "enum", "instanceof", "return", "transient",
        "catch", "extends", "int", "short", "try",
        "char", "final", "interface", "static", "void",
        "class", "finally", "long", "strictfp", "volatile",
        "const", "float", "native", "super", "while", "_"};
        keywords = new HashSet<>(Arrays.asList(keywordsList));
    }

    private void readNext() throws IOException {
        next = (char) System.in.read();
        pos++;
    }
    
    Token getNextToken() throws IOException {
        // Skip whitespaces, tabs, new-lines
        while (next == ' ' || next == '\n' || next == '\t') {
            if (next == '\n') {
                line += 1;
                pos = 1;
            }
            readNext();
        }

        // Numerical (integer) literals
        if (Character.isDigit(next)) {
            int numValue = 0;
            do {
                numValue = numValue * 10 + Character.digit(next, 10);
                readNext();
            } while (Character.isDigit(next));
            return new Num(numValue, line, pos);
        }

        // Identifiers and keywords
        if (Character.isJavaIdentifierStart(next)) {
            StringBuilder wordBuilder = new StringBuilder();
            do {
                wordBuilder.append(next);
                readNext();
            } while (Character.isJavaIdentifierPart(next));
            String wordString = wordBuilder.toString();
            if (keywords.contains(wordString))
                return new Word(Tag.ID, wordString, "keyword", line, pos);
            else
                return new Word(Tag.ID, wordString, "identifier", line, pos);
        }

        // Operators: '/' '/=' and comments '//' '/**/'
        if (next == '/') {
            readNext();
            if (next == '=')
                return new Operator("/=", line, pos);
            
            // If we have "//" then we skip the line and go to the next one
            else if (next == '/' ){
                while (next != '\n') {
                    readNext();
                }
                line += 1;
                pos = 1;
                readNext();
            }

            // If we have /* then we check for */
            else if (next == '*') {
                boolean asterisk_prev = false;
                readNext();
                while (next != '/' || !asterisk_prev) {
                    asterisk_prev = next == '*';
                    if (next == '\n') {
                        line += 1;
                        pos = 1;
                    }
                    readNext();
                }
                readNext();
            }

            // If we have / and anything else afterwards
            else 
                return new Operator("/", line, pos);
        }


        // Operators: '?' '~'
        if (next == '?' || next == '~') {
            return new Operator(Character.toString(next), line, pos);
        }
        // Operators: '=' '=='
        if (next == '=') {
            readNext();
            if (next == '=')
                return new Operator("==", line, pos);
            else
                return new Operator("=", line, pos);
        }
        // Operators: '>' '>=' '>>' '>>=' '>>>' '>>>='
        if (next == '>') {
            readNext();
            if (next == '=')
                return new Operator(">=", line, pos);
            else {
                if (next == '>') {
                    readNext();
                    if (next == '=')
                        return new Operator(">>=", line, pos);
                    else {
                        if (next == '>') {
                            readNext();
                            if (next == '=')
                                return new Operator(">>>=", line, pos);
                            else
                                return new Operator(">>>", line, pos);
                        } else
                            return new Operator(">>", line, pos);
                    }
                } else
                    return new Operator(">", line, pos);
            }
        }
        // Operators: '<' '<=' '<<' '<<='
        if (next == '<') {
            readNext();
            if (next == '=')
                return new Operator("<=", line, pos);
            else {
                readNext();
                if (next != '<')
                    return new Operator("<", line, pos);
                else {
                    readNext();
                    if (next == '=')
                        return new Operator("<<=", line, pos);
                    else {
                        return new Operator("<<", line, pos);
                    }
                }
            }
        }
        // Operators: '!' '!='
        if (next == '!') {
            readNext();
            if (next == '=')
                return new Operator("!=", line, pos);
            else
                return new Operator("!", line, pos);
        }
        // Operators: '&' '&&' '&='
        if (next == '&') {
            readNext();
            if (next == '&')
                return new Operator("&&", line, pos);
            if (next == '=')
                return new Operator("&=", line, pos);
            else
                return new Operator("&", line, pos);
        }
        // Operators: '|' '||' '|='
        if (next == '|') {
            readNext();
            if (next == '|')
                return new Operator("||", line, pos);
            if (next == '=')
                return new Operator("|=", line, pos);
            else
                return new Operator("|", line, pos);
        }
        // Operators: '+' '++' '+='
        if (next == '+') {
            readNext();
            if (next == '+')
                return new Operator("++", line, pos);
            if (next == '=')
                return new Operator("+=", line, pos);
            else
                return new Operator("+", line, pos);
        }
        // Operators: '-' '--' '-=' '->'
        if (next == '-') {
            readNext();
            if (next == '-')
                return new Operator("--", line, pos);
            if (next == '=')
                return new Operator("-=", line, pos);
            if (next == '>')
                return new Operator("->", line, pos);
            else
                return new Operator("-", line, pos);
        }
        // Operators: '*' '*='
        if (next == '*') {
            readNext();
            if (next == '=')
                return new Operator("*=", line, pos);
            else
                return new Operator("*", line, pos);
        }

        // Operators: '^' '^='
        if (next == '^') {
            readNext();
            if (next == '=')
                return new Operator("^=", line, pos);
            else
                return new Operator("^", line, pos);
        }
        // Operators: '%' '%='
        if (next == '%') {
            readNext();
            if (next == '=')
                return new Operator("%=", line, pos);
            else
                return new Operator("%", line, pos);
        }
        // Operator or delimiter: ':' '::'
        if (next == ':') {
            readNext();
            if (next == ':')
                return new Delimiter("::", line, pos);
            else
                return new Operator(":", line, pos);
        }
        // Delimiters: '.' '...'
        if (next == '.') {
            readNext();
            if (next != '.') {
                return new Delimiter(".", line, pos);
            } else {
                readNext();
                if (next == '.') {
                    return new Delimiter("...", line, pos);
                } else {
                    return new Token(Tag.UNRECOGNIZED, pos, line);
                }
            }
        }
        // Delimiters: '(' ')' '{' '}' '[' ']' ';' '.' ',' '@'
        if (next == '(' || next == ')' || next == '{' || next == '}' || next == '[' || next == ']' || next == ';' ||
                next == ',' || next == '@' || next == '\'' || next == '"') {
            char delimiter = next;
            readNext();
            return new Delimiter(Character.toString(delimiter), line, pos);
        }
        return new Token(Tag.UNRECOGNIZED, pos, line);
    }
}
