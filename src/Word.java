class Word extends Token {
    final String lexeme;
    final String type;

    Word(int wordTag, String lexeme, String type, int line, int pos) {
        super(wordTag, line, pos);
        this.lexeme = lexeme;
        this.type = type;
    }
}
