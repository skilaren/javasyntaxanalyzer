class Num extends Token {
    final int value;

    Num(int value, int line, int pos) {
        super(Tag.NUM, line, pos);
        this.value = value;
    }
}
